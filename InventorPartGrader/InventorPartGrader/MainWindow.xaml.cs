﻿using System.Windows;

namespace InventorPartGrader {
	public partial class MainWindow : Window {
		public MainWindow() {
			InitializeComponent();
			Content = new MainPage();
		}
	}
}
