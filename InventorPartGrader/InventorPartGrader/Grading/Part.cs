﻿using System;
using System.IO;
using System.Windows;
using Microsoft.WindowsAPICodePack.Shell;

namespace InventorPartGrader.Grading {
	internal class Part {
		#region static

		public static Part[] MassConstruct(string[] filenames) {
			Part[] parts = new Part[filenames.Length];

			for (var i = 0; i < parts.Length; i++) {
				parts[i] = new Part(filenames[i]);
			}

			return parts;
		}

		protected static double GetError(double key, double test) {
			return 100.0d * ((key - test) / key);
		}

		protected static bool CheckError(double error, int tolerance) {
			return error <= tolerance;
		}

		#endregion static

		public string Filepath { get; protected set; }
		public string Filename { get; protected set; }
		public string Status { get; set; }

		public Part(string filepath) {
			Filepath = filepath;
			Filename = Path.GetFileName(Filepath);
		}

		#region Document

		protected Inventor.Document Document;

		private bool Open() {
			try {
				if (Document != null && Document.Open) {
					return true;
				}
			} catch {
				// Document was open, but then closed
				// Reopen it below
			}

			try {
				Document = App.Grading.Server.Documents.Open(Filepath, true);
				return true;
			} catch {
				return false;
			}
		}

		private Inventor.PropertySet SummaryInformation {
			// http://modthemachine.typepad.com/files/ipropertiesandparameters.pdf

			get {
				return Document.PropertySets["Inventor Summary Information"];
			}
		}

		#endregion Document

		#region Author

		private string author;

		public string Author {
			get {
				if (!string.IsNullOrEmpty(author) || !Open()) {
					return author;
				}

				return author = (string) SummaryInformation.ItemByPropId[4].Value;
			}
		}

		public bool HasDuplicateAuthor {
			get {
				return App.Grading.DuplicateAuthors.Contains(Author);
			}
		}

		#endregion Author

		#region Center of Gravity

		private Inventor.Point masscenter;
		private bool? masscenterpassed;

		public Inventor.Point MassCenter {
			get {
				if (masscenter != null || !Open()) {
					return masscenter;
				}

				return masscenter = ((Inventor.PartDocument) Document).ComponentDefinition.MassProperties.CenterOfMass;
			}
		}

		public string MassCenterCoords {
			get {
				if (MassCenter == null) {
					return string.Empty;
				}

				return string.Format("X = {0}\nY = {1}\nZ = {2}", MassCenter.X, MassCenter.Y, MassCenter.Z);
			}
		}

		public bool MassCenterPassed {
			get {
				if (masscenterpassed.HasValue || !Open()) {
					return masscenterpassed.Value;
				}

				// Strict mode on?

				if (App.Grading.Strict) {
					var distXYZ = App.Grading.KeyPart.MassCenter.DistanceTo(MassCenter);
					return (bool) (masscenterpassed = (distXYZ <= App.Grading.Tolerance));
				}

				// Strict mode off?

				var coords = new double[3];
				MassCenter.GetPointData(ref coords);

				int[,] combinations = {
					{ 0, 1, 2 }, // X Y Z
					{ 0, 2, 1 }, // X Z Y
					{ 1, 0, 2 }, // Y Z X
					{ 1, 2, 0 }, // Y X Z
					{ 2, 0, 1 }, // Z X Y
					{ 2, 1, 0 }, // Z Y X
				};

				var coordsCopy = new double[3];
				var pointCopy = MassCenter.Copy();
				var lengths = new double[combinations.GetLength(0)];

				for (int c = 0; c < combinations.GetLength(0); c++) {
					coordsCopy = new double[] {
						coords[combinations[c, 0]],
						coords[combinations[c, 1]],
						coords[combinations[c, 2]]
					};

					pointCopy.PutPointData(coordsCopy);
					lengths[c] = App.Grading.KeyPart.MassCenter.DistanceTo(pointCopy);
				}

				Array.Sort(lengths);
				return (bool) (masscenterpassed = (lengths[0] <= App.Grading.Tolerance));
			}
		}

		#endregion Center of Gravity

		#region Preview

		private System.Windows.Media.ImageSource preview;

		public System.Windows.Media.ImageSource Preview {
			get {
				if (preview != null || !Open()) {
					return preview;
				}

				var thumbnail = ShellFile.FromFilePath(Filepath).Thumbnail;
				thumbnail.CurrentSize = new Size(96, 96);
				return preview = thumbnail.BitmapSource;
			}
		}

		#endregion Preview

		#region SurfaceArea

		private double surfacearea = -1;

		public double SurfaceArea {
			get {
				if (surfacearea >= 0 || !Open()) {
					return surfacearea;
				}

				return surfacearea = ((Inventor.PartDocument) Document).ComponentDefinition.MassProperties.Area;
			}
		}

		public double SurfaceAreaError {
			get {
				return GetError(App.Grading.KeyPart.SurfaceArea, SurfaceArea);
			}
		}

		public bool SurfaceAreaPassed {
			get {
				return Math.Abs(SurfaceAreaError) <= App.Grading.Tolerance;
			}
		}

		#endregion SurfaceArea

		#region Volume

		private double volume = -1;

		public double Volume {
			get {
				if (volume >= 0 || !Open()) {
					return volume;
				}

				return volume = ((Inventor.PartDocument) Document).ComponentDefinition.MassProperties.Volume;
			}
		}

		public double VolumeError {
			get {
				return GetError(App.Grading.KeyPart.Volume, Volume);
			}
		}

		public bool VolumePassed {
			get {
				return Math.Abs(VolumeError) <= App.Grading.Tolerance;
			}
		}

		#endregion Volume
	}
}
