﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;

namespace InventorPartGrader.Grading {
	internal class Run {
		private static readonly string ProgId = "Inventor.Application";

		public Inventor.Application Server { get; private set; }

		public List<string> Authors { get; private set; }
		public List<string> DuplicateAuthors { get; private set; }

		public Part KeyPart { get; protected set; }
		public Part[] GradeParts { get; protected set; }
		public int Tolerance { get; protected set; }
		public bool Strict { get; protected set; }

		public Run(string keyPart, string[] gradeParts, int tolerance, bool strict) {
			KeyPart = new Part(keyPart);
			GradeParts = Part.MassConstruct(gradeParts);
			Tolerance = tolerance;
			Strict = strict;

			Authors = new List<string>();
			DuplicateAuthors = new List<string>();

			try {
				// Find an existing instance
				Server = (Inventor.Application) Marshal.GetActiveObject(ProgId);
			} catch {
				try {
					// Start a new instance
					System.Type type = System.Type.GetTypeFromProgID(ProgId);
					Server = (Inventor.Application) System.Activator.CreateInstance(type);
				} catch (System.Exception exception) {
					throw exception;
				}
			}

			Server.Visible = true;
			Server.SilentOperation = true;
		}

		public delegate void GradeStatus(float percentComplete);

		public void Start(GradeStatus callback) {
			int graded = 0;

			foreach (var part in GradeParts) {
				// Track duplicate authors
				if (Authors.Contains(part.Author)) {
					DuplicateAuthors.Add(part.Author);
				} else {
					Authors.Add(part.Author);
				}

				graded++;
				callback(graded / GradeParts.Length);
				Task.Yield();
			}

			try {
				Server.Documents.CloseAll();
			} catch {
				MessageBox.Show(Properties.Resources.CrashWarning, string.Empty, MessageBoxButton.OK, MessageBoxImage.Exclamation);
			}
		}
	}
}
