﻿using Microsoft.WindowsAPICodePack.Shell;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Forms = System.Windows.Forms;

namespace InventorPartGrader {
	public partial class MainPage : Page {
		private string KeyPart;
		private string[] GradeParts = { };
		private int Tolerance = 0;
		private bool Strict = false;

		public MainPage() {
			InitializeComponent();

			// Load saved settings
			var settings = Properties.Settings.Default;
			KeyPart = settings.KeyPart;
			ToleranceSlider.Value = Tolerance = settings.Tolerance;
			StrictToggle.IsChecked = Strict = settings.StrictMode;

			// Initialize control states
			RunButton_Update();
			KeyPartLabel_Update();
			GradingPartsLabel_Update();
		}

		private void SaveSettings() {
			var settings = Properties.Settings.Default;
			settings.KeyPart = KeyPart;
			settings.Tolerance = Tolerance;
			settings.StrictMode = Strict;
			settings.Save();
		}

		private void KeyPartLabel_MouseUp(object sender, MouseButtonEventArgs e) {
			var file = PromptSingleFileSelection();

			if (!String.IsNullOrEmpty(file)) {
				KeyPart = file;
			}

			KeyPartLabel_Update();
			RunButton_Update();
		}

		private void KeyPartLabel_Update() {
			// Update text
			KeyPartLabel.Content = String.IsNullOrEmpty(KeyPart)
				? "(none)"
				: System.IO.Path.GetFileName(KeyPart);

			// Update icon
			try {
				var icon = ShellFile.FromFilePath(KeyPart).Thumbnail;
				icon.CurrentSize = new Size(32, 32);
				KeyPartIcon.Source = icon.BitmapSource;
			} catch {
				KeyPartIcon.Source = null;
			}
		}

		private void GradingPartsLabel_MouseUp(object sender, MouseButtonEventArgs e) {
			var files = PromptFileSelection();

			if (files.Length > 0) {
				GradeParts = files;
			}

			GradingPartsLabel_Update();
			RunButton_Update();
		}

		private void GradingPartsLabel_Update() {
			int n = GradeParts.Length;		
			GradingPartsLabel.Content = n.ToString() + " file" + (n == 1 ? "" : "s") + " selected";
		}

		private string PromptSingleFileSelection() {
			try {
				return PromptFileSelection(false)[0];
			} catch {
				return null;
			}
		}

		private string[] PromptFileSelection(bool multiselect = true) {
			using (var dialog = new Forms.OpenFileDialog()) {
				dialog.DefaultExt = "ipt";
				dialog.Filter = "Inventor Parts|*.ipt|All Files|*.*";
				dialog.Multiselect = multiselect;
				dialog.ReadOnlyChecked = true;

				dialog.ShowDialog();
				return dialog.FileNames;
			}
		}

		private void ToleranceSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
			Tolerance = (int) ToleranceSlider.Value;
			ToleranceLabel_Update();
		}

		private void ToleranceLabel_Update() {
			ToleranceLabel.Content = string.Format("±{0}%", Tolerance);
		}

		private void StrictToggle_Checked(object sender, RoutedEventArgs e) {
			Strict = true;
		}

		private void StrictToggle_Unchecked(object sender, RoutedEventArgs e) {
			Strict = false;
		}

		private void RunButton_Click(object sender, RoutedEventArgs e) {
			SaveSettings();

			if (GradeParts.Contains(KeyPart)) {
				var result = MessageBox.Show(Properties.Resources.KeyInGrading, string.Empty, MessageBoxButton.YesNo, MessageBoxImage.Warning);

				if (result == MessageBoxResult.No) {
					return;
				}
			}

			try {
				App.Grading = new Grading.Run(KeyPart, GradeParts, Tolerance, Strict);
			} catch {
				MessageBox.Show(Properties.Resources.InventorNotStarted, string.Empty, MessageBoxButton.OK, MessageBoxImage.Error);
				return;
			}

			Application.Current.MainWindow.Content = new GradingPage();
		}

		private void RunButton_Update() {
			RunButton.IsEnabled = !string.IsNullOrEmpty(KeyPart) && GradeParts.Length > 0;
		}

		private void SaveButton_Click(object sender, RoutedEventArgs e) {
			SaveSettings();
		}
	}
}
