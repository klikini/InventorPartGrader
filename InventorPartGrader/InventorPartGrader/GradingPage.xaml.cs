﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace InventorPartGrader {
	public partial class GradingPage : Page {
		public GradingPage() {
			InitializeComponent();

			foreach (var part in App.Grading.GradeParts) {
				PartList.Items.Add(part);
			}
		}

		private async void Page_Loaded(object sender, RoutedEventArgs e) {
			await Task.Run(() => {
				App.Grading.Start((ratio) => {
					TotalProgress.Dispatcher.Invoke(() => {
						TotalProgress.Value = 100.0d * ratio;
					});

					PartList.Dispatcher.Invoke(() => {
						PartList.Items.Refresh();
					});
				});
			});
		}

		private void BackButton_Click(object sender, RoutedEventArgs e) {
			Application.Current.MainWindow.Content = new MainPage();
		}
	}
}
